const express = require("express")
const path = require("path")
const app = express()
const http = require("http")
const server = http.createServer(app)
const { Server } = require("socket.io")
const io = new Server(server)
const { PORT = 3000 } = process.env
let onlineUsers = []
let currentStocks = {
    "ABC Traders" : 44.51,
    "Trendy FinTech" : 23.99,
    "SuperCorp" : 126.91
}

server.listen(PORT, () => console.log(`Server started at port ${PORT}`))

app.use(express.static(path.join(__dirname, "public", "static")))
app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.get("/", (req, res) => res.sendFile(path.join(__dirname, "public", "index.html")))

app.patch("/update", (req, res) => {
    const { data } = req.body
    //Throw error if patched stock does not exist in currentStocks variable
    if (!verifyExistingStock(data, currentStocks)) {
        console.log("Error: incoming patch data did not contain a valid stock name.")
        return res.status(400).send("Error: recieved stock does not exist in the server database. Did you spell the stock correctly?")
    }
    //Only update stock if the stock price actually is new:
    if(verifyNewStockPrice(data, currentStocks)){
        updateCurrentStockPrices(data, currentStocks)
        io.emit("updateAllCurrentStocks", currentStocks)
        io.emit("updateLatestStockEvent", data)
        return res.json(data)
    }
    //Respond with an empty JSON object if the price isn't updated
    res.json()
})

app.get("/current", (req, res) => res.json(currentStocks))

io.on("connection", (socket) => {
    socket.emit("updateCurrentStockEvent", currentStocks)
    //User handling bellow, not part of assignment.
    socket.emit("updateAllCurrentStocks", currentStocks)
    socket.emit("addConnectedUser", socket.id)
    socket.emit("sendCurrentOnlineUsers", onlineUsers)
    socket.broadcast.emit("broadcastNewOnlineUser", socket.id)
    console.log(`User with ID: ${socket.id} joined the server`)
    onlineUsers.push(socket.id)
    
    socket.on("disconnect", () => removeOnlineUser(onlineUsers, socket.id))
})

//Verifies that the recieved stock object is an existing stockName in the currentStock variable.
const verifyExistingStock = (receivedStock, currentStocks) => receivedStock.stockName in currentStocks

//Verifies that the recieved stockprice is new.
const verifyNewStockPrice = (receivedStock, currentStocks) => receivedStock.price !== currentStocks[receivedStock.stockName]

//Update the affected stock with a new price.
const updateCurrentStockPrices = (receivedStock, currentStocks) => currentStocks[receivedStock.stockName] = receivedStock.price

//Remove newly connected user from onlineUsers variable & frontpage list
const removeOnlineUser = (onlineUsers, socketId) => {
    console.log(`User with ID: ${socketId} left the server`)
    onlineUsers.splice(onlineUsers.indexOf(socketId), 1)
    io.emit("removeOnlineUser", socketId)
}