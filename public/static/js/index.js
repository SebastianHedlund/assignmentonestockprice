const socket = io()
const stockPrice = document.getElementById("event-stock-price-ul")
const abcTradesPrice = document.getElementById("abc-traders-price")
const trendyFinTechPrice = document.getElementById("trendy-fintech-price")
const superCorpPrice = document.getElementById("supercorp-price")

socket.on('updateLatestStockEvent', (data) =>{
    const stockEvent = document.createElement('li')
    stockEvent.setAttribute("class", "event-stock-price")
    stockEvent.textContent = `${data.stockName} has been updated, the new price is $${data.price}`
    stockPrice.insertBefore(stockEvent, stockPrice.firstChild)
})

socket.on('updateCurrentStockEvent', (currentStocks) =>{
    for (const stock in currentStocks) {
        const stockEvent = document.createElement('li')
        stockEvent.setAttribute("class", "event-stock-price")
        stockEvent.textContent = `The current price of ${stock} is $${currentStocks[stock]}`
        stockPrice.insertBefore(stockEvent, stockPrice.firstChild)
    }
})

socket.on('updateAllCurrentStocks', (newStock) =>{
    abcTradesPrice.textContent = "$" + newStock["ABC Traders"]
    trendyFinTechPrice.textContent = "$" + newStock["Trendy FinTech"]
    superCorpPrice.textContent = "$" + newStock["SuperCorp"]
})

//User handling bellow, not part of assignment
const onlineUsersList = document.getElementById("online-users-ul")

socket.on("broadcastNewOnlineUser", socketId => {
    onlineUsersList.appendChild(createUserListElement(socketId)) 
})

socket.on("removeOnlineUser", (socketId) => {
    onlineUsersList.removeChild(Array.from(onlineUsersList.childNodes).find(text => text.innerHTML === socketId))
})

socket.on("sendCurrentOnlineUsers", (onlineUsers) => {
    onlineUsers.map(user => {
        const currentLogin = document.createElement('li')
        currentLogin.setAttribute("class", "online-users")
        currentLogin.textContent = user
        onlineUsersList.appendChild(currentLogin)
    })
})

socket.on("addConnectedUser", (socketId) => {
    const newUser = createUserListElement(socketId)
    newUser.style.color="white"
    onlineUsersList.appendChild(newUser) 
})

const createUserListElement = (socketId) => {
    const user = document.createElement('li')
    user.setAttribute("class", "online-users")
    user.textContent = socketId
    return user
}